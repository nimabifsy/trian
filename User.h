//
// Created by ziyu on 18-4-29.
//

#ifndef MYPROJECT_USER_H
#define MYPROJECT_USER_H
#include <iostream>
#include <cstring>
#include <cstdio>
#include "MyAlgorithm.h"
#include <fstream>
#include "DB.h"
#include <map>
#include "train.h"
using namespace std;
#define USER_FILE_NAME "user_data_name"
#define USER_FILE_ID "user_data_id"
#define START_ID 2018
#define USER_DB_BRANCHES 4
#define USER_DB_B_NAME 4

enum privilige{NOT,NOR, ADM};

class UserDB{
    public:
    class User{
    public:
        String name; //!!
        char password[21];
        char email[21];
        char phone[21];
        privilige p;



        User(char *_name, char *_password, char *_email, char *_phone, privilige _p):
                name(_name) {

            clear(password);
            clear(email);
            clear(phone);
            strcpy(password, _password);
            strcpy(email, _email);
            strcpy(phone, _phone);
            p = _p;
        }
        User() {}
        ~User() {}
    };
    // map<size_t, User> User_database_by_id;
    BplusTree<size_t, User, 4096> User_database_by_id;
    size_t cur_size;
public:
    UserDB():cur_size(0), User_database_by_id(USER_FILE_ID, true){}
    long long Register(char *_name, char *_password, char *_email, char *_phone) {
        privilige p = cur_size ? (privilige)1 : (privilige) 2;

        // if(User_database_by_name.Search(_name).first) {cout<<" " << _name ;return -1;}

        User_database_by_id.insert(START_ID + cur_size, User(_name, _password, _email, _phone, p));
        // User_database_by_name.Insert(_name ,START_ID + cur_size);


        return START_ID + cur_size++;
    }
    int Login(size_t id, char *_password) {
        // User u(User_database_by_id.Search(id).second);
        auto tmp = User_database_by_id.find(id);
        if(tmp == User_database_by_id.end()) return 0;
        if(!strcmp(tmp.getdata().second.password, _password)) return 1;
        else return 0;
    }

    //我和文档上的写的不一样，我直接返回来一个bool 和 User的pair
    int query_profile(size_t id, char *ans) {
        auto t = User_database_by_id.find(id);
        if(t == User_database_by_id.end()) return 0;
        memset(ans, 0, sizeof(ans));
        strcat(ans, t.getdata().second.name.data);
        strcat(ans, " ");
        strcat(ans, t.getdata().second.email);
        strcat(ans, " ");
        strcat(ans, t.getdata().second.phone);
        strcat(ans, " ");
        strcat(ans, int_to_string((int)t.getdata().second.p));
        return 1;
    }

    int modify_profile(size_t id, char *_name, char *_password, char *_email, char *_phone) {
        auto t = User_database_by_id.find(id);

        if(t == User_database_by_id.end())
            return 0;
        else
            User_database_by_id.update(id,User(_name, _password, _email, _phone, t.getdata().second.p));
        return 1;
    }

    bool modify_privilege(size_t id1, size_t id2, privilige pri) {
        // if(id1 == id2) return false;
        auto p1 = User_database_by_id.find(id1);
        auto p2 = User_database_by_id.find(id2);

        if(p1 == User_database_by_id.end() || p2 == User_database_by_id.end()) return false;
        if(p1.getdata().second.p != 2) return false;
        if(p2.getdata().second.p == 2 && pri != 2) return false;
        User tmp = p2.getdata().second;
        tmp.p = pri;
        User_database_by_id.update(id2, tmp);
        return true;
    }
};
#endif //MYPROJECT_USER_H
