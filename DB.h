//pay attention that you haven't write a destructor for bptree
#ifndef BPLUSTREE_H
#define BPLUSTREE_H

#include <cstring>
#include <fstream>

//#define NDEBUG

#include <cassert>

class stack {
   private:
    struct node {
        int value;
        node *next;

        node(int val = 0, node *ne = nullptr) : value(val), next(ne) {}
    };

   public:
    node *head;

    stack() : head(new node) {}

    void insert(int x) {
        node *p = new node(x, head->next);
        head->next = p;
    }

    // without checking size == 0, check it outside using empty;
    int remove() {
        node *ptmp = head->next;
        int headvalue = ptmp->value;
        head->next = ptmp->next;
        delete (ptmp);
        return headvalue;
    }

    bool empty() { return head->next == nullptr; }

    void initialize() {
        node *p = head->next;
        node *ptmp;
        while (p != nullptr) {
            ptmp = p;
            p = p->next;
            delete ptmp;
        }
        head->next = nullptr;
        insert(0);
    }
};

template <class key_t, class value_t, int BLOCK_SIZE>
class BplusTree;

template <class key_t, int BLOCK_SIZE>
class InternalNode {
   public:
    const static int MIN_POINTER =
        (BLOCK_SIZE - sizeof(int) - sizeof(int) + sizeof(key_t)) /
        ((sizeof(key_t) + sizeof(int)) * 2);
    const static int MAX_POINTER = 2 * MIN_POINTER;
    int self;
    int count;
    key_t key[MAX_POINTER - 1];
    int pointer[MAX_POINTER];

    /*print sizeof((*this)) to check whether the statement works*/
    InternalNode() { memset(this, 0, sizeof((*this))); }

    int upper_bound(const key_t &keya) const {
        if (count == 1) return 0;
        int left = 0;
        int right = count - 2;
        int middle;
        while (right > left) {
            middle = left + (right - left) / 2;
            if (key[middle] > keya) {
                right = middle;
            } else {
                left = middle + 1;
            }
        }
        if (key[right] > keya)
            return right;
        else
            return count - 1;
    }

    // without any check
    void simple_insert(const key_t &keya, int pright) {
        int j;
        j = upper_bound(keya);
        for (int i = count - 1; i > j; --i) {
            key[i] = key[i - 1];
            pointer[i + 1] = pointer[i];
        }
        key[j] = keya;
        pointer[j + 1] = pright;
        ++count;
    }
    // return -1 when there is no result
    /* how to improve the speed of the function since its linear search*/
    int get_index(int pos) const {
        int i = 0;
        for (; i < count; ++i) {
            if (pointer[i] == pos) break;
        }
        if (i == count) return -1;
        return i;
    }
};

template <class key_t, class value_t, int BLOCK_SIZE>
class LeafNode {
   public:
    const static int MIN_DATA =
        (BLOCK_SIZE - 3 * sizeof(int) - sizeof(int)) /
        ((sizeof(value_t) + sizeof(key_t)) * 2);
    const static int MAX_DATA = 2 * MIN_DATA;

    int pre;
    int next;
    int self;
    int count;
    key_t key[MAX_DATA];
    value_t value[MAX_DATA];

    LeafNode() { memset(this, 0, sizeof((*this))); }

    bool whether_exist(const key_t &keya) const {
        int i = lower_bound(keya);
        if (i == count or key[i] != keya)
            return false;
        else
            return true;
    }

    int upper_bound(const key_t &keya) const {
        if (count == 0) return 0;
        int left = 0;
        int right = count - 1;
        int middle;
        while (right > left) {
            middle = left + (right - left) / 2;
            if (key[middle] > keya) {
                right = middle;
            } else {
                left = middle + 1;
            }
        }
        if (key[right] > keya)
            return right;
        else {
            return count;
        }
    }

    int lower_bound(const key_t &keya) const {
        if (count == 0) return 0;
        int left = 0;
        int right = count - 1;
        int middle;
        while (right > left) {
            middle = left + (right - left) / 2;
            if (key[middle] < keya) {
                left = middle + 1;
            } else {
                right = middle;
            }
        }
        if (key[right] < keya) {
            return count;
        } else
            return right;
    }

    // without any check
    void simple_insert(const key_t &keya, const value_t &valuea) {
        int i = upper_bound(keya);
        for (int j = count; j > i; --j) {
            key[j] = key[j - 1];
            value[j] = value[j - 1];
        }
        key[i] = keya;
        value[i] = valuea;
        ++count;
    }

    // checks whether exist
    bool simple_delete(const key_t &keya) {
        int i = lower_bound(keya);
        if (i == count) return false;
        if (key[i] != keya) {
            return false;
        }
        for (int j = i; j < count - 1; ++j) {
            key[j] = key[j + 1];
            value[j] = value[j + 1];
        }
        --count;
        return true;
    }
};

template <class key_t, class value_t, int BLOCK_SIZE>
class iterator {
   public:
    /* how to use static leaf in mobanlei*/
    LeafNode<key_t, value_t, BLOCK_SIZE> leaf;
    BplusTree<key_t, value_t, BLOCK_SIZE> *ptree;
    int pblock;
    int currblock;
    int index;

    iterator(BplusTree<key_t, value_t, BLOCK_SIZE> *p, int pb = 0,
             int id = 0)
        : ptree(p), pblock(pb), currblock(0), index(id) {}

    bool operator==(const iterator &o) {
        return (pblock == o.pblock and index == o.index);
    }

    bool operator!=(const iterator &o) {
        return (pblock != o.pblock or index != o.index);
    }

    std::pair<key_t, value_t> getdata() {
        if (currblock != pblock) {
            ptree->fs.seekg(pblock);
            ptree->fs.read(reinterpret_cast<char *>(&leaf), sizeof(leaf));
        }
        return std::make_pair(leaf.key[index], leaf.value[index]);
    }

    iterator operator++(int) {
        if (pblock != currblock) {
            ptree->fs.seekg(pblock);
            ptree->fs.read(reinterpret_cast<char *>(&leaf), sizeof(leaf));
        }
        if (leaf.count - 1 == index) {
            pblock = leaf.next;
            index = 0;
        } else {
            ++index;
        }
        return (*this);
    }
};

template <class key_t, class value_t, int BLOCK_SIZE>
class BplusTree {
   public:
    struct inform_t {
        int height;
        int root_offset;
        int end_of_file;
        int leaf_offset;
    } inform;

    char path[20];
    std::fstream fs;
    stack fatherline;

    /* remember you can change the way of update and remove to iterator based to
     * improve*/
   public:
    BplusTree(const char *from, bool newfile);

    void initialize();

    iterator<key_t, value_t, BLOCK_SIZE> begin();

    iterator<key_t, value_t, BLOCK_SIZE> end();

    iterator<key_t, value_t, BLOCK_SIZE> lower_bound(const key_t &);

    iterator<key_t, value_t, BLOCK_SIZE> find(const key_t &);

    bool exist(const key_t &);

    bool update(const key_t &, const value_t &);

    bool insert(const key_t &, const value_t &);

    int remove(const key_t &);

    value_t query(const key_t &);

    int search_leaf(const key_t &);

    key_t create_leaf(LeafNode<key_t, value_t, BLOCK_SIZE> &leaf,
                      LeafNode<key_t, value_t, BLOCK_SIZE> &newleaf);

    key_t create_internal(InternalNode<key_t, BLOCK_SIZE> &internal,
                          InternalNode<key_t, BLOCK_SIZE> &newinternal);

    int internal_insert(int pdad, const key_t &key, int pleft,
                        int pright);

    bool internal_borrow_left(InternalNode<key_t, BLOCK_SIZE> &right,
                              InternalNode<key_t, BLOCK_SIZE> &dad, int index);

    bool internal_borrow_right(InternalNode<key_t, BLOCK_SIZE> &left,
                               InternalNode<key_t, BLOCK_SIZE> &dad, int index);

    bool internal_combine(InternalNode<key_t, BLOCK_SIZE> &internal,
                          InternalNode<key_t, BLOCK_SIZE> &dad, int index);

    bool leaf_borrow_right(LeafNode<key_t, value_t, BLOCK_SIZE> &left,
                           InternalNode<key_t, BLOCK_SIZE> &dad, int index);

    bool leaf_borrow_left(LeafNode<key_t, value_t, BLOCK_SIZE> &right,
                          InternalNode<key_t, BLOCK_SIZE> &dad, int index);

    bool leaf_combine(LeafNode<key_t, value_t, BLOCK_SIZE> &leaf,
                      InternalNode<key_t, BLOCK_SIZE> &dad, int index);

    int internal_delete(InternalNode<key_t, BLOCK_SIZE> &curr, int index,
                        bool flag);

    int allocleaf(LeafNode<key_t, value_t, BLOCK_SIZE> &leaf) {
        leaf.count = 0;
        leaf.self = inform.end_of_file;
        inform.end_of_file += BLOCK_SIZE;
        return leaf.self;
    }

    int allocinternal(InternalNode<key_t, BLOCK_SIZE> &internal) {
        internal.count = 1;
        internal.self = inform.end_of_file;
        inform.end_of_file += BLOCK_SIZE;
        return internal.self;
    }

    /* fill them later on if you want to reduce the size of file*/
    void unallocleaf(LeafNode<key_t, value_t, BLOCK_SIZE> &p) {}

    void unallocinternal(InternalNode<key_t, BLOCK_SIZE> &p) {}

    void readinform() {
        fs.seekg(0);
        fs.read(reinterpret_cast<char *>(&inform), sizeof(inform_t));
    }

    void readleaf(LeafNode<key_t, value_t, BLOCK_SIZE> *p, int position) {
        fs.seekg(position);
        fs.read(reinterpret_cast<char *>(p),
                sizeof(LeafNode<key_t, value_t, BLOCK_SIZE>));
    }

    void readinternal(InternalNode<key_t, BLOCK_SIZE> *p, int position) {
        fs.seekg(position);
        fs.read(reinterpret_cast<char *>(p),
                sizeof(InternalNode<key_t, BLOCK_SIZE>));
    }

    void writeinform() {
        fs.seekp(0);
        fs.write(reinterpret_cast<char *>(&inform), sizeof(inform_t));
    }

    void writeleaf(LeafNode<key_t, value_t, BLOCK_SIZE> *p) {
        fs.seekp(p->self);
        fs.write(reinterpret_cast<char *>(p),
                 sizeof(LeafNode<key_t, value_t, BLOCK_SIZE>));
    }

    void writeinternal(InternalNode<key_t, BLOCK_SIZE> *p) {
        fs.seekp(p->self);
        fs.write(reinterpret_cast<char *>(p),
                 sizeof(InternalNode<key_t, BLOCK_SIZE>));
    }
};

/*when destruct remember to use write infomr*/
template <class key_t, class value_t, int BLOCK_SIZE>
BplusTree<key_t, value_t, BLOCK_SIZE>::BplusTree(const char *from,
                                                 bool newfile) {
    memset(path, 0, sizeof(path));
    if (newfile) {
        strcpy(path, from);
        fs.open(path, std::fstream::out);
        fs.close();
		fs.open(path);
        initialize();
    } else {
        strcpy(path, from);
        readinform();
    }
}

template <class key_t, class value_t, int BLOCK_SIZE>
void BplusTree<key_t, value_t, BLOCK_SIZE>::initialize() {
    memset(&inform, 0, sizeof(inform_t));
    inform.height = 1;
    inform.end_of_file = BLOCK_SIZE;

    InternalNode<key_t, BLOCK_SIZE> root;
    inform.root_offset = allocinternal(root);
    LeafNode<key_t, value_t, BLOCK_SIZE> leaf;
    root.pointer[0] = inform.leaf_offset = allocleaf(leaf);
    writeleaf(&leaf);
    writeinternal(&root);
}

template <class key_t, class value_t, int BLOCK_SIZE>
iterator<key_t, value_t, BLOCK_SIZE>
BplusTree<key_t, value_t, BLOCK_SIZE>::begin() {
    return iterator<key_t, value_t, BLOCK_SIZE>(this, inform.leaf_offset, 0);
}

template <class key_t, class value_t, int BLOCK_SIZE>
iterator<key_t, value_t, BLOCK_SIZE>
BplusTree<key_t, value_t, BLOCK_SIZE>::end() {
    return iterator<key_t, value_t, BLOCK_SIZE>(this, 0, 0);
}

template <class key_t, class value_t, int BLOCK_SIZE>
iterator<key_t, value_t, BLOCK_SIZE>
BplusTree<key_t, value_t, BLOCK_SIZE>::lower_bound(const key_t &keya) {
    int pleaf = search_leaf(keya);
    LeafNode<key_t, value_t, BLOCK_SIZE> leaf;
    readleaf(&leaf, pleaf);
    int i = leaf.lower_bound(keya);
    if (i == leaf.count)
        return iterator<key_t, value_t, BLOCK_SIZE>(this, leaf.next, 0);
    else
        return iterator<key_t, value_t, BLOCK_SIZE>(this, pleaf, i);
}

template <class key_t, class value_t, int BLOCK_SIZE>
iterator<key_t, value_t, BLOCK_SIZE>
BplusTree<key_t, value_t, BLOCK_SIZE>::find(const key_t &keya) {
    int pleaf = search_leaf(keya);
    LeafNode<key_t, value_t, BLOCK_SIZE> leaf;
    readleaf(&leaf, pleaf);
    int i = leaf.lower_bound(keya);
    if (i == leaf.count or leaf.key[i] != keya)
        return iterator<key_t, value_t, BLOCK_SIZE>(this, 0, 0);
    else
        return iterator<key_t, value_t, BLOCK_SIZE>(this, pleaf, i);
};

template <class key_t, class value_t, int BLOCK_SIZE>
bool BplusTree<key_t, value_t, BLOCK_SIZE>::exist(const key_t &keya) {
    int pleaf = search_leaf(keya);
    LeafNode<key_t, value_t, BLOCK_SIZE> leaf;
    readleaf(&leaf, pleaf);
    return leaf.whether_exist(keya);
}

// don't check anything
template <class key_t, class value_t, int BLOCK_SIZE>
value_t BplusTree<key_t, value_t, BLOCK_SIZE>::query(const key_t &keya) {
    int pleaf = search_leaf(keya);
    LeafNode<key_t, value_t, BLOCK_SIZE> leaf;
    readleaf(&leaf, pleaf);
    int i = leaf.lower_bound(keya);
    return leaf.value[i];
};

template <class key_t, class value_t, int BLOCK_SIZE>
bool BplusTree<key_t, value_t, BLOCK_SIZE>::update(const key_t &keya,
                                                   const value_t &value) {
    int pleaf = search_leaf(keya);
    LeafNode<key_t, value_t, BLOCK_SIZE> leaf;
    readleaf(&leaf, pleaf);
    int i = leaf.lower_bound(keya);
    if (i == leaf.count) return false;
    if (leaf.key[i] != keya) return false;
    leaf.value[i] = value;
    writeleaf(&leaf);
    return true;
}

template <class key_t, class value_t, int BLOCK_SIZE>
bool BplusTree<key_t, value_t, BLOCK_SIZE>::insert(const key_t &key,
                                                   const value_t &value) {
    fatherline.initialize();
    int pleaf = search_leaf(key);
    LeafNode<key_t, value_t, BLOCK_SIZE> leaf;
    readleaf(&leaf, pleaf);
    // insert failure, you can optimize a little
    if (leaf.whether_exist(key)) return false;
    if (leaf.count == leaf.MAX_DATA) {
        LeafNode<key_t, value_t, BLOCK_SIZE> newleaf;
        allocleaf(newleaf);
        key_t split_key = create_leaf(leaf, newleaf);
        if (key < split_key) {
            leaf.simple_insert(key, value);
        } else {
            newleaf.simple_insert(key, value);
        }
        writeleaf(&leaf);
        writeleaf(&newleaf);
        // insert to dad;
        internal_insert(fatherline.remove(), split_key, leaf.self,
                        newleaf.self);
    } else {
        leaf.simple_insert(key, value);
        writeleaf(&leaf);
    }
    return true;
}

template <class key_t, class value_t, int BLOCK_SIZE>
int BplusTree<key_t, value_t, BLOCK_SIZE>::remove(const key_t &key) {
    fatherline.initialize();
    int pleaf = search_leaf(key);
    LeafNode<key_t, value_t, BLOCK_SIZE> leaf;
    readleaf(&leaf, pleaf);
    if (!leaf.simple_delete(key)) return 0;

    if (leaf.count < leaf.MIN_DATA) {
        InternalNode<key_t, BLOCK_SIZE> dad;
        readinternal(&dad, fatherline.remove());
        int index = dad.get_index(leaf.self);
        //        assert(index != -1);

        // deal when there is only one leaf and one internal as root;
        if (inform.root_offset == dad.self and dad.count == 1) {
            //            assert(index == 0);
            //            assert(inform.height == 1);
            writeleaf(&leaf);
            return 1;
        }
        if (leaf_borrow_left(leaf, dad, index)) return 2;
        if (leaf_borrow_right(leaf, dad, index)) return 3;
        bool flag = leaf_combine(leaf, dad, index);
        internal_delete(dad, index, flag);
        return 4;
    } else {
        writeleaf(&leaf);
        return 5;
    }
}

template <class key_t, class value_t, int BLOCK_SIZE>
int BplusTree<key_t, value_t, BLOCK_SIZE>::search_leaf(const key_t &key) {
    int height = inform.height;
    int pcurr = inform.root_offset;
    InternalNode<key_t, BLOCK_SIZE> node;
    int j;
    while (height > 0) {
        readinternal(&node, pcurr);
        fatherline.insert(pcurr);
        j = node.upper_bound(key);
        pcurr = node.pointer[j];
        --height;
    }
    return pcurr;
}

template <class key_t, class value_t, int BLOCK_SIZE>
key_t BplusTree<key_t, value_t, BLOCK_SIZE>::create_leaf(
    LeafNode<key_t, value_t, BLOCK_SIZE> &leaf,
    LeafNode<key_t, value_t, BLOCK_SIZE> &newleaf) {
    int j = 0;
    for (int i = leaf.MIN_DATA; i < leaf.MAX_DATA; ++i) {
        newleaf.key[j] = leaf.key[i];
        newleaf.value[j] = leaf.value[i];
        ++j;
    }
    leaf.count = leaf.MIN_DATA;
    newleaf.count = newleaf.MIN_DATA;

    newleaf.next = leaf.next;
    newleaf.pre = leaf.self;
    leaf.next = newleaf.self;
    if (newleaf.next != 0) {
        LeafNode<key_t, value_t, BLOCK_SIZE> old_next;
        readleaf(&old_next, newleaf.next);
        old_next.pre = newleaf.self;
        writeleaf(&old_next);
    }
    return newleaf.key[0];
}

template <class key_t, class value_t, int BLOCK_SIZE>
key_t BplusTree<key_t, value_t, BLOCK_SIZE>::create_internal(
    InternalNode<key_t, BLOCK_SIZE> &internal,
    InternalNode<key_t, BLOCK_SIZE> &newinternal) {
    int j = 0;
    for (int i = internal.MIN_POINTER; i < internal.MAX_POINTER - 1; ++i) {
        newinternal.pointer[j] = internal.pointer[i];
        newinternal.key[j] = internal.key[i];
        ++j;
    }
    newinternal.pointer[newinternal.MIN_POINTER - 1] =
        internal.pointer[internal.MAX_POINTER - 1];
    internal.count = internal.MIN_POINTER;
    newinternal.count = newinternal.MIN_POINTER;
    return internal.key[internal.MIN_POINTER - 1];
}

template <class key_t, class value_t, int BLOCK_SIZE>
int BplusTree<key_t, value_t, BLOCK_SIZE>::internal_insert(int pdad,
                                                           const key_t &key,
                                                           int pleft,
                                                           int pright) {
    if (pdad == 0) {
        InternalNode<key_t, BLOCK_SIZE> newroot;
        inform.root_offset = allocinternal(newroot);
        ++inform.height;

        ++newroot.count;
        //        assert(newroot.count == 2);
        newroot.key[0] = key;
        newroot.pointer[0] = pleft;
        newroot.pointer[1] = pright;
        writeinternal(&newroot);
        return 1;
    }
    InternalNode<key_t, BLOCK_SIZE> internal;
    readinternal(&internal, pdad);
    if (internal.count == internal.MAX_POINTER) {
        InternalNode<key_t, BLOCK_SIZE> new_internal;
        allocinternal(new_internal);
        key_t split_key = create_internal(internal, new_internal);
        if (key < split_key) {
            internal.simple_insert(key, pright);
        } else {
            new_internal.simple_insert(key, pright);
        }
        writeinternal(&internal);
        writeinternal(&new_internal);

        internal_insert(fatherline.remove(), split_key, internal.self,
                        new_internal.self);
        return 2;
    } else {
        internal.simple_insert(key, pright);
        writeinternal(&internal);
        return 3;
    }
}

template <class key_t, class value_t, int BLOCK_SIZE>
bool BplusTree<key_t, value_t, BLOCK_SIZE>::internal_borrow_right(
    InternalNode<key_t, BLOCK_SIZE> &left, InternalNode<key_t, BLOCK_SIZE> &dad,
    int index) {
    if (index == dad.count - 1) return false;
    InternalNode<key_t, BLOCK_SIZE> right;
    readinternal(&right, dad.pointer[index + 1]);
    if (right.count == right.MIN_POINTER)
        return false;
    else {
        left.pointer[left.count] = right.pointer[0];
        left.key[left.count - 1] = dad.key[index];
        dad.key[index] = right.key[0];
        for (int i = 0; i < right.count - 2; ++i) {
            right.pointer[i] = right.pointer[i + 1];
            right.key[i] = right.key[i + 1];
        }
        right.pointer[right.count - 2] = right.pointer[right.count - 1];
        --right.count;
        ++left.count;
        //        assert(left.count == left.MIN_POINTER);
        writeinternal(&left);
        writeinternal(&right);
        writeinternal(&dad);
        return true;
    }
}

template <class key_t, class value_t, int BLOCK_SIZE>
bool BplusTree<key_t, value_t, BLOCK_SIZE>::internal_borrow_left(
    InternalNode<key_t, BLOCK_SIZE> &right,
    InternalNode<key_t, BLOCK_SIZE> &dad, int index) {
    if (index == 0) return false;
    InternalNode<key_t, BLOCK_SIZE> left;
    readinternal(&left, dad.pointer[index - 1]);
    if (left.count == left.MIN_POINTER)
        return false;
    else {
        for (int i = right.count - 1; i > 0; --i) {
            right.key[i] = right.key[i - 1];
            right.pointer[i + 1] = right.pointer[i];
        }
        right.pointer[1] = right.pointer[0];
        right.pointer[0] = left.pointer[left.count - 1];
        right.key[0] = dad.key[index - 1];
        dad.key[index - 1] = left.key[left.count - 2];
        ++right.count;
        //        assert(right.count == right.MIN_POINTER);
        --left.count;
        writeinternal(&left);
        writeinternal(&right);
        writeinternal(&dad);
        return true;
    }
}

template <class key_t, class value_t, int BLOCK_SIZE>
bool BplusTree<key_t, value_t, BLOCK_SIZE>::internal_combine(
    InternalNode<key_t, BLOCK_SIZE> &internal,
    InternalNode<key_t, BLOCK_SIZE> &dad, int index) {
    // right_combine
    if (index != dad.count - 1) {
        InternalNode<key_t, BLOCK_SIZE> right;
        readinternal(&right, dad.pointer[index + 1]);
        int j = internal.count;
        for (int i = 0; i < right.count - 1; ++i) {
            internal.key[j] = right.key[i];
            internal.pointer[j + 1] = right.pointer[i + 1];
            ++j;
        }
        internal.key[internal.count - 1] = dad.key[index];
        internal.pointer[internal.count] = right.pointer[0];
        internal.count += right.count;
        // deal with child
        unallocinternal(right);
        writeinternal(&internal);
        return true;
    } else {  // combine with left
        InternalNode<key_t, BLOCK_SIZE> left;
        readinternal(&left, dad.pointer[index - 1]);
        int j = left.count;
        for (int i = 0; i < internal.count - 1; ++i) {
            left.key[j] = internal.key[i];
            left.pointer[j + 1] = internal.pointer[i + 1];
            ++j;
        }
        left.key[left.count - 1] = dad.key[index - 1];
        left.pointer[left.count] = internal.pointer[0];
        left.count += internal.count;
        unallocinternal(internal);
        writeinternal(&left);
        return false;
    }
}

template <class key_t, class value_t, int BLOCK_SIZE>
bool BplusTree<key_t, value_t, BLOCK_SIZE>::leaf_borrow_left(
    LeafNode<key_t, value_t, BLOCK_SIZE> &right,
    InternalNode<key_t, BLOCK_SIZE> &dad, int index) {
    if (index == 0) return false;

    LeafNode<key_t, value_t, BLOCK_SIZE> left;
    readleaf(&left, dad.pointer[index - 1]);
    //    assert(left.count >= left.MIN_DATA);
    if (left.count == left.MIN_DATA)
        return false;
    else {
        for (int i = right.count; i > 0; --i) {
            right.key[i] = right.key[i - 1];
            right.value[i] = right.value[i - 1];
        }
        right.key[0] = left.key[left.count - 1];
        right.value[0] = left.value[left.count - 1];
        dad.key[index - 1] = right.key[0];
        ++right.count;
        //        assert(right.count == right.MIN_DATA);
        --left.count;
        writeleaf(&left);
        writeleaf(&right);
        writeinternal(&dad);
        return true;
    }
}

template <class key_t, class value_t, int BLOCK_SIZE>
bool BplusTree<key_t, value_t, BLOCK_SIZE>::leaf_borrow_right(
    LeafNode<key_t, value_t, BLOCK_SIZE> &left,
    InternalNode<key_t, BLOCK_SIZE> &dad, int index) {
    if (index == dad.count - 1) return false;
    LeafNode<key_t, value_t, BLOCK_SIZE> right;
    readleaf(&right, dad.pointer[index + 1]);
    if (right.count == right.MIN_DATA) {
        return false;
    } else {
        left.key[left.count] = right.key[0];
        left.value[left.count] = right.value[0];
        for (int i = 0; i < right.count - 1; ++i) {
            right.key[i] = right.key[i + 1];
            right.value[i] = right.value[i + 1];
        }
        dad.key[index] = right.key[0];
        ++left.count;
        //        assert(left.count == left.MIN_DATA);
        --right.count;
        writeleaf(&left);
        writeleaf(&right);
        writeinternal(&dad);
        return true;
    }
}

template <class key_t, class value_t, int BLOCK_SIZE>
bool BplusTree<key_t, value_t, BLOCK_SIZE>::leaf_combine(
    LeafNode<key_t, value_t, BLOCK_SIZE> &leaf,
    InternalNode<key_t, BLOCK_SIZE> &dad, int index) {
    // combine with right by default;
    if (index != dad.count - 1) {
        LeafNode<key_t, value_t, BLOCK_SIZE> right;
        readleaf(&right, dad.pointer[index + 1]);
        int i = leaf.count;
        for (int j = 0; j < right.count; ++j) {
            leaf.key[i] = right.key[j];
            leaf.value[i] = right.value[j];
            ++i;
        }
        leaf.count += right.count;
        leaf.next = right.next;
        if (right.next != 0) {
            LeafNode<key_t, value_t, BLOCK_SIZE> new_next;
            readleaf(&new_next, right.next);
            new_next.pre = leaf.self;
            writeleaf(&new_next);
        }
        unallocleaf(right);
        writeleaf(&leaf);
        return true;
    } else {
        LeafNode<key_t, value_t, BLOCK_SIZE> left;
        readleaf(&left, dad.pointer[index - 1]);
        int i = left.count;
        //        assert(left.count == left.MIN_DATA);
        for (int j = 0; j < leaf.count; ++j) {
            left.key[i] = leaf.key[j];
            left.value[i] = leaf.value[j];
            ++i;
        }
        left.count += leaf.count;
        left.next = leaf.next;
        if (leaf.next != 0) {
            LeafNode<key_t, value_t, BLOCK_SIZE> new_next;
            readleaf(&new_next, leaf.next);
            new_next.pre = left.self;
            writeleaf(&new_next);
        }
        unallocleaf(leaf);
        writeleaf(&left);
        return false;
    }
}

// delete the key and next pointer with keyorder index;
template <class key_t, class value_t, int BLOCK_SIZE>
int BplusTree<key_t, value_t, BLOCK_SIZE>::internal_delete(
    InternalNode<key_t, BLOCK_SIZE> &curr, int index, bool flag) {
    if (flag) {
        for (int i = index; i < curr.count - 2; ++i) {
            curr.key[i] = curr.key[i + 1];
            curr.pointer[i + 1] = curr.pointer[i + 2];
        }
        --curr.count;
    } else {
        for (int i = index - 1; i < curr.count - 2; ++i) {
            curr.key[i] = curr.key[i + 1];
            curr.pointer[i + 1] = curr.pointer[i + 2];
        }
        --curr.count;
    }
    /* here deserve checking*/
    if (inform.root_offset == curr.self) {
        if (inform.height != 1 and curr.count == 1) {
            inform.root_offset = curr.pointer[0];
            --inform.height;
            unallocinternal(curr);
            return 1;
        } else {
            writeinternal(&curr);
            return 2;
        }
    } else {
        if (curr.count < curr.MIN_POINTER) {
            InternalNode<key_t, BLOCK_SIZE> dad;
            readinternal(&dad, fatherline.remove());
            int indexa = dad.get_index(curr.self);
            //            assert(indexa != -1);
            if (internal_borrow_left(curr, dad, indexa)) return 3;
            if (internal_borrow_right(curr, dad, indexa)) return 4;
            bool flaga = internal_combine(curr, dad, indexa);
            internal_delete(dad, indexa, flaga);
            return 5;
        } else {
            writeinternal(&curr);
            return 6;
        }
    }
}

#endif