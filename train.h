//
// Created by aaaalex on 18-5-29.
//

#ifndef UNTITLED_TRAIN_H
#define UNTITLED_TRAIN_H
#include "DB.h"
#include <iostream>
#include "MyAlgorithm.h"
#include <algorithm>
#define TRAIN_FILE "train_file"
#define QUERY_DB "query_file"
#define BUY_DB "buy_file"



using namespace std;

void clear(char *str) { memset(str, 0, sizeof(str)); }

// void copy(char des[], char *p) {
// 	if (sizeof(des) < strlen(p))
// 		cerr << "length error" << endl;
// 	strcpy(des, p);
// }


char num[20];
char * float_to_string(float d) {
	memset(num, 0, sizeof(num));
	int a = d;
	int weishu = 1;

	while (a / 10) {
		a /= 10;
		weishu++;
	}
	a = d;
	int i = weishu;
	while (i) {
		num[--i] = (int)a % 10 + '0';
		a /= 10;
	}
	num[weishu] = '.';
	// d*=10;
	// num[weishu+1] = (int)d % 10 + '0';
	// d*=10;
	// num[weishu+2] = (int)d % 10 + '0';

	for (int i = 0; i < 2; i++) {
		d *= 10;
		num[weishu + i + 1] = (int)d % 10 + '0';
	}
	return num;
}

char * int_to_string(int a) {
	memset(num, 0, sizeof(num));
	int d = a;
	int weishu = 1;
	while (d / 10) {
		d /= 10;
		weishu++;
	}
	d = a;
	while (weishu) {
		num[--weishu] = d % 10 + '0';
		d /= 10;
	}
	return num;
}

class TrainDB {


	struct TicketIndex {
		//station ---.getdata(). train_id
		char train_id[21];
		char station[40];
		bool operator<(const TicketIndex &o)const {
			if (strcmp(station, o.station) < 0) return true;
			else if (strcmp(station, o.station) == 0)
				if (strcmp(train_id, o.train_id) < 0) return true;

			return false;
		}
		bool operator>=(const TicketIndex &o)const { return !(*this < o); }

		bool operator>(const TicketIndex &o)const {
			if (strcmp(station, o.station) > 0) return true;
			else if (strcmp(station, o.station) == 0)
				if (strcmp(train_id, o.train_id) > 0) return true;

			return false;
		}

		bool operator<=(const TicketIndex &o)const { return !(*this > o); }
		bool operator!=(const TicketIndex &o)const { return (*this > o) || (*this < o); }
		bool operator==(const TicketIndex &o)const { return !(*this != o); }

		TicketIndex(char *_id, char *_station) {
			clear(train_id);
			clear(station);

			strcpy(train_id, _id);
			strcpy(station, _station);
		}
		TicketIndex() = default;
	};

	struct TicketValue{
		char catalog[10];
		TicketValue(char *c) {
			memset(catalog, 0, 10);
			strcpy(catalog, c);
		}
		TicketValue(const TicketValue &o) {
			memset(catalog, 0, 10);
			strcpy(catalog, o.catalog);
		}

		TicketValue operator=(const TicketValue &o) {
			memset(catalog, 0, 10);
			strcpy(catalog, o.catalog);
			return *this;
		}
		TicketValue() {memset(catalog, 0, 10);}
	};

	struct BuyTicketIndex {
		size_t user_id;
		int date;
		char train_id[20];
		char loc1[40], loc2[40];
		BuyTicketIndex(size_t u, char *t, int d, char *l1, char *l2) {
			clear(train_id);
			strcpy(train_id, t);
			user_id = u;
			// clear(ticket_kind);
			clear(loc1);
			clear(loc2);
			date = d;
			// strcpy(ticket_kind, tk);
			strcpy(loc1, l1);
			strcpy(loc2, l2);
		}
		BuyTicketIndex() { user_id = 0; }

		bool operator<(const BuyTicketIndex &o)const {
			if (user_id < o.user_id) return true;
			else if (user_id == o.user_id) {
				if (date < o.date) return true;
				else if (date == o.date) {
					if (strcmp(train_id, o.train_id) < 0) return true;
					else if (strcmp(train_id, o.train_id) == 0) {
						if (strcmp(loc1, o.loc1) < 0) return true;
						else if (strcmp(loc1, o.loc1) == 0) {
							if (strcmp(loc2, o.loc2) < 0) return true;
							// else if(strcmp(loc2, o.loc2) == 0) {
							//     if(strcmp(ticket_kind, o.ticket_kind) < 0) return true;
							// }
						}
					}
				}
			}
			return false;
		}

		bool operator>(const BuyTicketIndex &o)const {
			if (user_id > o.user_id) return true;
			else if (user_id == o.user_id) {
				if (date > o.date) return true;
				else if (date == o.date) {
					if (strcmp(train_id, o.train_id) > 0) return true;
					else if (strcmp(train_id, o.train_id) == 0) {
						if (strcmp(loc1, o.loc1) > 0) return true;
						else if (strcmp(loc1, o.loc1) == 0) {
							if (strcmp(loc2, o.loc2) > 0) return true;
							// else if(strcmp(loc2, o.loc2) == 0) {
							//     if(strcmp(ticket_kind, o.ticket_kind) > 0) return true;
							// }
						}
					}
				}
			}
			return false;
		}

		bool operator>= (const BuyTicketIndex &o)const { return !(*this < o); }
		bool operator!=(const BuyTicketIndex &o)const { return (*this > o) || (*this < o); }
		bool operator==(const BuyTicketIndex &o)const { return !(*this != o); }
	};

	struct BuyTicketValue {
		int num[5];
		char ticket_kind[5][20];
		BuyTicketValue(int n[], char _ticket_kind[][20]) {
			for (int i = 0; i < 5; i++) {
				num[i] = n[i];
				clear(ticket_kind[i]);
				strcpy(ticket_kind[i], _ticket_kind[i]);
				//                cerr<< "警告 "<<ticket_kind[i]<< endl;
			}
		}
		BuyTicketValue() = default;
	};

	bool nextDay(char * a, char *b) {
		if (a[0] < b[0]) return false;
		else if (a[0] == b[0]) {
			if (a[1] < b[1]) return false;
			else if (a[1] == b[1]) {
				if (a[3] < b[3]) return false;
				else if (a[3] == b[3]) {
					if (a[4] < b[4]) return false;
				}
			}
		}
		return true;
	}


public:

	class Ticket_i {
		public:

		int date;
		String id;
		Ticket_i(int d, char *i) {
			id = i;
			date = d;
			
		}
		Ticket_i(const Ticket_i &o) {
			id = o.id;
			date = o.date;
		}
		Ticket_i() {
			date = 0;
		}
		Ticket_i operator=(const Ticket_i &o) {
			if(this == &o) return *this;
			id = o.id;
			date = o.date;
			return *this;
		}

		bool operator< (const Ticket_i &o)const {
			if(id < o.id) return true;
			else if(id == o.id && date < o.date) return true;
			return false;
		}
		bool operator> (const Ticket_i &o)const {return (o < *this);}
		bool operator<= (const Ticket_i &o)const {return !(*this > o);}
		bool operator>= (const Ticket_i &o)const {return !(*this < o);}
		bool operator== (const Ticket_i &o)const {return (*this >= o && *this <= o);}
		bool operator!= (const Ticket_i &o)const {return !(*this == o);}
	};

	class Ticket{
	public:

		int ticket[50][5];
		Ticket(int t[][5]) {

			for(int i = 0 ; i < 50; i++)
				for(int j = 0; j < 5; j++)
					ticket[i][j] = t[i][j];
		}
		Ticket(const Ticket &o) {

			for(int i = 0 ; i < 50; i++)
				for(int j = 0; j < 5; j++)
					ticket[i][j] = o.ticket[i][j];
		}
		Ticket() {
			for(int i = 0 ; i < 50; i++)
				for(int j = 0; j < 5; j++)
					ticket[i][j] = 2000;
		}

	};


	class Train {
	public:
		char train_id[20];
		char name[40];
		char catalog[10];
		int num_station;
		int num_price;
		char ticket_kind[5][20];
		char station_name[50][40];
		char arrive_time[50][10];
		char start_time[50][10];
		char stop_over_time[50][10];
		float price[50][5];
		bool OnSale;

		// Train() = default;
		Train(char *_train_id, char *_name, char *_catalog, int _num_station,
			int _num_price, char _ticket_kind[][20], char _station_name[][40], char _arrive_time[][10],
			char _start_time[][10], char _stop_over_time[][10], float _price[][5]) {

			memset(train_id, 0, sizeof(train_id));
			memset(name, 0, sizeof(name));
			memset(catalog, 0, sizeof(catalog));


			strcpy(train_id, _train_id);
			strcpy(name, _name);
			strcpy(catalog, _catalog);
			num_station = _num_station;
			num_price = _num_price;
			for (auto i = 0; i < num_price; i++) {
				memset(ticket_kind[i], 0, sizeof(ticket_kind[i]));
				strcpy(ticket_kind[i], _ticket_kind[i]);
			}
			for (auto i = 0; i < num_station; i++) {
				memset(station_name[i], 0, sizeof(station_name[i]));
				memset(arrive_time[i], 0, sizeof(arrive_time[i]));
				memset(start_time[i], 0, sizeof(start_time[i]));
				memset(stop_over_time[i], 0, sizeof(stop_over_time[i]));
				strcpy(station_name[i], _station_name[i]);
				strcpy(arrive_time[i], _arrive_time[i]);
				strcpy(start_time[i], _start_time[i]);
				strcpy(stop_over_time[i], _stop_over_time[i]);
				for (int j = 0; j < num_price; j++)
					price[i][j] = _price[i][j];
			}

			OnSale = false;
		}
		Train() = default;
		Train(const Train &o) {
			memset(train_id, 0, sizeof(train_id));
			memset(name, 0, sizeof(name));
			memset(catalog, 0, sizeof(catalog));

			strcpy(train_id, o.train_id);
			strcpy(name, o.name);
			strcpy(catalog, o.catalog);
			num_station = o.num_station;
			num_price = o.num_price;
			for (auto i = 0; i < num_price; i++) {
				memset(ticket_kind[i], 0, sizeof(ticket_kind[i]));
				strcpy(ticket_kind[i], o.ticket_kind[i]);
			}
			for (auto i = 0; i < num_station; i++) {
				memset(station_name[i], 0, sizeof(station_name[i]));
				memset(arrive_time[i], 0, sizeof(arrive_time[i]));
				memset(start_time[i], 0, sizeof(start_time[i]));
				memset(stop_over_time[i], 0, sizeof(stop_over_time[i]));
				strcpy(station_name[i], o.station_name[i]);
				strcpy(arrive_time[i], o.arrive_time[i]);
				strcpy(start_time[i], o.start_time[i]);
				strcpy(stop_over_time[i], o.stop_over_time[i]);
				for (int j = 0; j < num_price; j++)
					price[i][j] = o.price[i][j];
			}

			OnSale = o.OnSale;
		}

		Train operator=(const Train &o) {
			memset(train_id, 0, sizeof(train_id));
			memset(name, 0, sizeof(name));
			memset(catalog, 0, sizeof(catalog));

			strcpy(train_id, o.train_id);
			strcpy(name, o.name);
			strcpy(catalog, o.catalog);
			num_station = o.num_station;
			num_price = o.num_price;
			for (auto i = 0; i < num_price; i++) {
				memset(ticket_kind[i], 0, sizeof(ticket_kind[i]));
				strcpy(ticket_kind[i], o.ticket_kind[i]);
			}
			for (auto i = 0; i < num_station; i++) {
				memset(station_name[i], 0, sizeof(station_name[i]));
				memset(arrive_time[i], 0, sizeof(arrive_time[i]));
				memset(start_time[i], 0, sizeof(start_time[i]));
				memset(stop_over_time[i], 0, sizeof(stop_over_time[i]));
				strcpy(station_name[i], o.station_name[i]);
				strcpy(arrive_time[i], o.arrive_time[i]);
				strcpy(start_time[i], o.start_time[i]);
				strcpy(stop_over_time[i], o.stop_over_time[i]);
				for (int j = 0; j < num_price; j++)
					price[i][j] = o.price[i][j];
			}


			OnSale = o.OnSale;
			return *this;
		}

		void print() {
			cout << train_id << " " << name << " " << catalog << " " << num_station << " " << num_price << " ";
			for (int i = 0; i < num_price; i++) cout << ticket_kind[i] << " ";
			cout << endl;
			for (auto i = 0; i < num_station; i++) {
				cout << station_name[i] << " " << arrive_time[i] << " " << start_time[i] << " " << stop_over_time[i] << " ";
				for (int j = 0; j < num_price; j++)
					cout << "￥" << price[i][j] << " ";
				cout << endl;
			}
		}
	};


	// map< String, Train> train_data_base;
	// map<TicketIndex , String> query_ticket_db; //staion ---.getdata(). train_id
	// map<BuyTicketIndex, BuyTicketValue> buy_ticket_db;

	BplusTree<String, Train, 4096 * 5> train_data_base;
	BplusTree<TicketIndex, TicketValue, 4096> query_ticket_db;
	BplusTree<BuyTicketIndex, BuyTicketValue, 4096 * 5> buy_ticket_db;
	BplusTree<Ticket_i, Ticket, 4096> ticket_db;

	TrainDB() :train_data_base(TRAIN_FILE, true), query_ticket_db(QUERY_DB, true), buy_ticket_db(BUY_DB, true), ticket_db("ticket_db", true) {};


	bool in_order(char *loc1, char *loc2, char *train_id) {
		Train t = train_data_base.find(train_id).getdata().second;
		int p1 = -1, p2 = -1;
		for (int i = 0; i < t.num_station; i++) {
			if (strcmp(t.station_name[i], loc1) == 0) p1 = i;
			if (strcmp(t.station_name[i], loc2) == 0)
				if (p1 == -1) return false;
		}
		return true;
	}


	int add_train(const Train &t) {
		if (train_data_base.find(t.train_id) != train_data_base.end())  return 0;
		train_data_base.insert(t.train_id, t);
		return 1;
	}


	int sale_train(char *_id) {
		auto t = train_data_base.find(_id);
		if (t == train_data_base.end()) return 0;
		if (t.getdata().second.OnSale) return 0;
		Train tmp = t.getdata().second;
		tmp.OnSale = true;
		train_data_base.update(_id, tmp);

		for (auto i = 0; i < t.getdata().second.num_station; i++) {
			        //    cerr << "insert "<< t.getdata().second.station_name[i]<<" "<< tmp.train_id<< "\t";
			query_ticket_db.insert(TicketIndex(tmp.train_id, tmp.station_name[i]
				), tmp.catalog);
		}
		for(int i = 1; i <= 30; i++) {
			ticket_db.insert(Ticket_i(i, _id), Ticket());
		}
		return 1;
	}

	void query_train(char *_id) {
		auto t = train_data_base.find(_id);
		if (t == train_data_base.end() || !t.getdata().second.OnSale) {
			cout << "0" << endl;
			return;
		}
		t.getdata().second.print();
	}

	int delete_train(char *_id) {
		auto t = train_data_base.find(_id);
		if (t == train_data_base.end()) return 0;
		if (t.getdata().second.OnSale) return 0;
		train_data_base.remove(_id);
		return 1;
	}

	int modify_train(const Train &train) {
		auto t = train_data_base.find(train.train_id);
		if (t == train_data_base.end()) return 0;
		if (t.getdata().second.OnSale) return 0;
		train_data_base.update(train.train_id, train);
		return 1;
	}

	//ans[100][200]
	int query_ticket(char *loc1, char *loc2, int date, char *catalog, char ans[][300]) {
		if (strcmp(loc1, loc2) == 0) return 0;
		//找出所有的可能的train_id
		int num_of_ans = 0;

		for (int i = 0; i < 200; i++) memset(ans[i], 0, sizeof(ans[i]));

		char ans_id[200][40];
		auto t1 = query_ticket_db.lower_bound(TicketIndex((char*)"", loc1));
		auto t2 = query_ticket_db.lower_bound(TicketIndex((char*)"", loc2));

		while (t1 != query_ticket_db.end() && t2 != query_ticket_db.end() &&
			strcmp(t1.getdata().first.station, loc1) == 0 && strcmp(t2.getdata().first.station, loc2) == 0) {
			// cerr<< "now exam "<<t1.getdata().first.train_id<< " "<< t2.getdata().first.train_id<< endl;
			// cerr<< t1.pblock<< " "<< t1.index<< endl;
			if (strcmp(t1.getdata().first.train_id, t2.getdata().first.train_id) < 0)
				t1++;
				
			else if (strcmp(t1.getdata().first.train_id, t2.getdata().first.train_id) > 0)
				t2++;
				
			else {
				for (int i = 0; i < strlen(catalog); i++) {
					for (int j = 0; j < strlen(t1.getdata().second.catalog); j++) {
						if (catalog[i] == t1.getdata().second.catalog[j]) {
							auto the_train = train_data_base.find(String((char*)t1.getdata().first.train_id)).getdata().second;
							int p1 = -1, p2 = -1;
							bool flag = true;
							//先判断顺序是不是反了
							for (int k = 0; k < the_train.num_station; k++) {
								if (strcmp(the_train.station_name[k], loc1) == 0) p1 = k;
								if (strcmp(the_train.station_name[k], loc2) == 0) {
									if (p1 == -1) { flag = false; break; }
								}
							}
							if (flag) {
								for (int kk = 0; kk < num_of_ans; kk++) {
									if (strcmp(ans_id[kk], t1.getdata().first.train_id) == 0) { flag = false; break; }
								}
								if (!flag) continue;
								strcpy(ans_id[num_of_ans++], t1.getdata().first.train_id);
								// cerr<< t1.getdata().first.train_id<< endl;
								break;
							}
						}
					}
				}
				t1++;
				t2++;
			}
		}

		//将答案输出到实现给定的数组之中，同时计算票价。
		for (int i = 0; i < num_of_ans; i++) {
			int p1 = -1, p2 = -1;
			auto t = train_data_base.find(ans_id[i]);
			for (auto j = 0; j < t.getdata().second.num_station; j++) {
				if (strcmp(t.getdata().second.station_name[j], loc1) == 0) p1 = j;
				if (strcmp(t.getdata().second.station_name[j], loc2) == 0) p2 = j;
			}

			strcat(ans[i], ans_id[i]);
			strcat(ans[i], " ");
			strcat(ans[i], loc1);
			strcat(ans[i], " ");
			char d[11] = "2018-06-", ch;
			d[8] = int(date / 10) + '0';
			d[9] = int(date % 10) + '0';
			strcat(ans[i], d);
			strcat(ans[i], " ");

			strcat(ans[i], t.getdata().second.start_time[p1]);
			strcat(ans[i], " ");
			strcat(ans[i], loc2);
			strcat(ans[i], " ");

			bool next_day = nextDay(t.getdata().second.start_time[p1], t.getdata().second.arrive_time[p2]);

			if (next_day) {
				if (d[9] - '9' == 0) { d[9] = '0', d[8]++; }
				else d[9]++;
			}
			strcat(ans[i], d);
			strcat(ans[i], " ");
			strcat(ans[i], t.getdata().second.arrive_time[p2]);


			//            cerr << p1<< " "<< p2<< endl;

			float price;
			for (int j = 0; j < t.getdata().second.num_price; j++) {
				price = 0;
				for (int k = p1 + 1; k <= p2; k++) {
					price += t.getdata().second.price[k][j];
				}
				strcat(ans[i], " ");
				strcat(ans[i], t.getdata().second.ticket_kind[j]);
				strcat(ans[i], " ");

				int num_of_ticket = 2000;


				//                cerr<< date<< " "<< t.getdata().second.ticket_kind[j]<< endl;


				// cerr<< p1<< " "<< p2<< " "<<date<< " ";
				for (int k = p1 + 1; k <= p2; k++) {
					auto tmp = ticket_db.find(Ticket_i(date ,t.getdata().second.train_id));
					num_of_ticket = min(num_of_ticket, tmp.getdata().second.ticket[k][j]);


					//                    cerr<< num_of_ticket<< endl;


				}
				// cerr<< num_of_ticket<< endl;

				strcat(ans[i], int_to_string(num_of_ticket));
				strcat(ans[i], " ");
				// cerr<< price<< endl;
				strcat(ans[i], float_to_string(price));


				//                cerr<< price<< endl;
			}
		}

		return num_of_ans;
	}

	int query_transfer(char *loc1, char *loc2, int date, char *catalog, char ans[][200]) {
		if (strcmp(loc1, loc2) == 0) return -1;


		char id_loc1[50][20], id_loc2[50][20], ans_id[20];
		int num1 = 0, num2 = 0, min_time = -1, cur_time = 0;
		memset(ans_id, 0, 20);
		for (int i = 0; i < 50; i++) {
			memset(id_loc1[i], 0, sizeof(id_loc1[i]));
			memset(id_loc2[i], 0, sizeof(id_loc2[i]));
		}

		auto it1 = query_ticket_db.lower_bound(TicketIndex((char*)"", loc1));
		auto it2 = query_ticket_db.lower_bound(TicketIndex((char*)"", loc2));

		while (strcmp(it1.getdata().first.station, loc1) == 0) {
			bool flag = false;
			for (int i = 0; i < strlen(catalog); i++) {
				for (int j = 0; j < strlen(it1.getdata().second.catalog); j++)
					if (catalog[i] == it1.getdata().second.catalog[j]) {
						strcpy(id_loc1[num1++], it1.getdata().first.train_id);
						// cout<< "添加1: "<< it1.getdata().first.train_id<<endl;
						flag = true;
						break;
					}
				if (flag) break;
			}

			if (it1 == query_ticket_db.end()) break;
			it1++;
		}

		while (strcmp(it2.getdata().first.station, loc2) == 0) {
			bool flag = false;
			for (int i = 0; i < strlen(catalog); i++) {
				for (int j = 0; j < strlen(it2.getdata().second.catalog); j++)
					if (catalog[i] == it2.getdata().second.catalog[j]) {
						strcpy(id_loc2[num2++], it2.getdata().first.train_id);
						// cout<< "添加2: "<< it1.getdata().first.train_id<<endl;
						flag = true;
						break;
					}
				if (flag) break;
			}
			if (it2 == query_ticket_db.end()) break;
			it2++;
		}
		if (num1 == 0 && num2 == 0) return -1;

		char location[40];
		char exam_id[50][20];
		int ex_num;
		for (auto it = query_ticket_db.begin(); it != query_ticket_db.end(); it++) {
			//warning

			// cout<< "目前为： "<< it.getdata().first.station<< endl;
			if (strcmp(loc1, it.getdata().first.station) == 0 || strcmp(it.getdata().first.station, loc2) == 0)
				if (it != query_ticket_db.end()) continue;
				else break;

				for (int i = 0; i < 50; i++)
					memset(exam_id[i], 0, sizeof(exam_id[i]));
				ex_num = 0;
				memset(location, 0, sizeof(location));

				strcpy(location, it.getdata().first.station);

				while (strcmp(location, it.getdata().first.station) == 0) {
					bool flag = false;
					for (int i = 0; i < strlen(catalog); i++) {
						for (int j = 0; j < strlen(it.getdata().second.catalog); j++)
							if (catalog[i] == it.getdata().second.catalog[j]) {
								strcpy(exam_id[ex_num++], it.getdata().first.train_id);
								//                            cout << "目前为："<< it.getdata().first.station<< "  " ;
								// cout << "目标添加： "<< it.getdata().first.train_id<< endl;
								flag = false;
								break;
							}
						if (flag) break;
					}
					if (it == query_ticket_db.end()) break;
					it++;
				}

				char ans_id1[20], ans_id2[20];
				memset(ans_id1, 0, sizeof(ans_id1));
				memset(ans_id2, 0, sizeof(ans_id2));
				bool flag1 = false, flag2 = false;

				// cout<< "location: "<< location<< endl;
				for (int i = 0; i < ex_num; i++) {
					for (int j = 0; j < num1; j++) {
						//                    cout << "当前测试： "<< exam_id[i]<<"  "<< id_loc1[j]<< endl;
						if (strcmp(exam_id[i], id_loc1[j]) == 0 && in_order(loc1, location, exam_id[i])) {
							// cout<< "继续加入ans_id1 ： "<< exam_id[i]<< endl;
							flag1 = true;
							strcpy(ans_id1, exam_id[i]);
							break;
						}
						if (flag1) break;
					}

					for (int j = 0; j < num2; j++) {
						//                    cout << "当前测试： "<< exam_id[i]<<"  "<< id_loc2[j]<< endl;
						if (strcmp(exam_id[i], id_loc2[j]) == 0 && in_order(location, loc2, exam_id[i])) {
							// cout<< "继续加入ans_id2 ： "<< exam_id[i]<< endl;
							flag2 = true;
							strcpy(ans_id2, exam_id[i]);
						}
					}
				}

				if (!(flag1 && flag2))
					if (it != query_ticket_db.end()) continue;
					else break;

					auto the_train2 = train_data_base.find(ans_id2).getdata().second;
					auto the_train1 = train_data_base.find(ans_id1).getdata().second;

					int p21 = -1, p22 = -1;
					for (int i = 0; i < the_train2.num_station; i++) {
						if (strcmp(location, the_train2.station_name[i]) == 0) p21 = i;
						if (strcmp(loc2, the_train2.station_name[i]) == 0) p22 = i;
					}
					if (p21 == -1 || p22 == -1)
						if (it != query_ticket_db.end()) continue;
						else break;

						int p11 = -1, p12 = -1;
						for (int i = 0; i < the_train1.num_station; i++) {
							if (strcmp(loc1, the_train1.station_name[i]) == 0) p11 = i;
							if (strcmp(location, the_train1.station_name[i]) == 0) p12 = i;
						}
						if (p11 == -1 || p12 == -1)
							if (it != query_ticket_db.end()) continue;
							else break;

							// cout << p11<< " "<< p12<< " "<< p21<< " "<< p22<< endl;

							cur_time = (10 * (the_train1.start_time[p11][0] - '0') +
								the_train1.start_time[p11][1] - '0') * 60 +
								10 * (the_train1.start_time[p11][3] - '0') +
								the_train2.arrive_time[p22][4] - '0' -
								(10 * (the_train2.arrive_time[p22][0] - '0') +
									the_train2.arrive_time[p22][1] - '0') * 60 +
								10 * (the_train2.arrive_time[p22][3] - '0') +
								the_train2.arrive_time[p22][4] - '0';

							if (cur_time < 0) cur_time += 24 * 3600;
							if (cur_time < min_time || min_time == -1) {
								min_time = cur_time;
								memset(ans[0], 0, sizeof(ans[0]));
								memset(ans[1], 0, sizeof(ans[1]));

								strcat(ans[0], ans_id1);
								strcat(ans[0], " ");
								strcat(ans[0], loc1);
								strcat(ans[0], " ");
								char d[11] = "2018-06-";
								d[8] = int(date / 10) + '0';
								d[9] = int(date % 10) + '0';
								strcat(ans[0], d);
								strcat(ans[0], " ");
								strcat(ans[0], the_train1.start_time[p11]);
								strcat(ans[0], " ");
								strcat(ans[0], location);
								strcat(ans[0], " ");
								strcat(ans[0], d);
								strcat(ans[0], " ");
								strcat(ans[0], the_train1.arrive_time[p12]);

								for (int i = 0; i < the_train1.num_price; i++) {
									float price = 0;
									for (int j = p11 + 1; j <= p12; j++) {
										price += the_train1.price[j][i];
									}
									strcat(ans[0], " ");
									strcat(ans[0], the_train1.ticket_kind[i]);
									strcat(ans[0], " ");
									int num_of_ticket = 2000;
									for (int j = p11 + 1; j <= p12; j++) {
										auto tmp = ticket_db.find(Ticket_i(date ,the_train1.train_id));
										num_of_ticket = min(num_of_ticket, tmp.getdata().second.ticket[j][i]);
										// num_of_ticket = min(num_of_ticket, the_train1.ticket[date][j][i]);
									}
									strcat(ans[0], int_to_string(num_of_ticket));
									strcat(ans[0], " ");
									strcat(ans[0], float_to_string(price));
								}

								strcat(ans[1], ans_id2);
								strcat(ans[1], " ");
								strcat(ans[1], location);
								strcat(ans[1], " ");

								strcat(ans[1], d);
								strcat(ans[1], " ");
								strcat(ans[1], the_train2.start_time[p21]);
								strcat(ans[1], " ");
								strcat(ans[1], loc2);
								strcat(ans[1], " ");
								strcat(ans[1], d);
								strcat(ans[1], " ");
								strcat(ans[1], the_train2.arrive_time[p22]);

								for (int i = 0; i < the_train2.num_price; i++) {
									float price = 0;
									for (int j = p21; j <= p22; j++) {
										price += the_train2.price[j][i];
									}
									strcat(ans[1], " ");
									strcat(ans[1], the_train2.ticket_kind[i]);
									strcat(ans[1], " ");
									int num_of_ticket = 2000;
									for (int j = p21 + 1; j <= p22; j++) {
										auto tmp = ticket_db.find(Ticket_i(date ,the_train2.train_id));
										num_of_ticket = min(num_of_ticket, tmp.getdata().second.ticket[j][i]);
										// num_of_ticket = min(num_of_ticket, the_train1.ticket[date][j][i]);
									}
									strcat(ans[1], int_to_string(num_of_ticket));
									strcat(ans[1], " ");
									strcat(ans[1], float_to_string(price));
								}
							}
							if (it != query_ticket_db.end()) break;
		}

		if (min_time == -1) return -1;
		else return 1;
	}



	int buy_ticket(size_t user_id, int num, char *train_id,
		char *loc1, char *loc2, int date, char *_ticket_kind) {
		auto t = train_data_base.find(train_id);
		if (t == train_data_base.end() || !t.getdata().second.OnSale) return 0;

		int p1 = -1, p2 = -1;
		for (int i = 0; i < t.getdata().second.num_station; i++) {
			//loc1 loc2 此处假定已满足先后顺序关系
			//            cerr<<i<< " "<< loc1<< " "<< loc2<< " "<<t.getdata().second.station_name[i]<< endl;
			if (strcmp(t.getdata().second.station_name[i], loc1) == 0) p1 = i;
			if (strcmp(t.getdata().second.station_name[i], loc2) == 0) p2 = i;
		}

		int tkn = -1;
		char tnk[5][20];
		for (int i = 0; i < t.getdata().second.num_price; i++) {
			clear(tnk[i]);
			strcpy(tnk[i], t.getdata().second.ticket_kind[i]);
			//    cerr << tnk[i]<< " ";
			if (strcmp(t.getdata().second.ticket_kind[i], _ticket_kind) == 0) tkn = i;
		}
		for (int i = t.getdata().second.num_price; i < 5; i++) {
			clear(tnk[i]);
		}
		if (tkn == -1) return 0;
		//    cerr<< tkn<< endl;
		int num_of_ticket = 2000;
		for (int i = p1 + 1; i <= p2; i++) {
			// num_of_ticket = min(num_of_ticket, t.getdata().second.ticket[date][i][tkn]);
			auto tmp = ticket_db.find(Ticket_i(date ,t.getdata().second.train_id));
			num_of_ticket = min(num_of_ticket, tmp.getdata().second.ticket[i][tkn]);

		}
		if (num_of_ticket >= num) {
			//            cerr<< "啊哈 "<< num<< " "<<p1<< " "<< p2<< endl;
			// auto tmp = t.getdata().second;
			auto tmp = ticket_db.find(Ticket_i(date ,t.getdata().second.train_id)).getdata().second;
			
			for (int i = p1 + 1; i <= p2; i++) tmp.ticket[i][tkn] -= num;
			ticket_db.update(Ticket_i(date ,t.getdata().second.train_id), tmp);

			int ans_num[5] = { 0 };
			ans_num[tkn] = num;
			//            cerr << user_id<< " "<< train_id<<" "<< date<< " "<< loc1<< " "<< loc2<< endl;
			buy_ticket_db.insert(BuyTicketIndex(user_id, train_id, date, loc1, loc2), BuyTicketValue(ans_num, tnk));
			return 1;
		}
		return 0;
	}

	int query_order(size_t id, int date, char *catalog, char ans[][200]) {
		char ans_id[100][20];
		int num_of_ans = 0;
		for (int i = 0; i < strlen(catalog); i++) {
			char c = catalog[i];
			auto it = buy_ticket_db.lower_bound(BuyTicketIndex(id, (char*)"", date, (char*)"", (char*)""));
			for (; it.getdata().first.date == date && it.getdata().first.user_id == id; it++) {
				bool flag = true;
				for (int j = 0; j < num_of_ans; j++)
					if (strcmp(it.getdata().first.train_id, ans_id[j]) == 0) flag = false;
				if (!flag) {
					if (it != buy_ticket_db.end()) continue;
					else break;
				}
				flag = false;
				auto p = train_data_base.find((char*)it.getdata().first.train_id).getdata().second;
				for (auto k = 0; k < strlen(p.catalog); k++)
					if (p.catalog[k] == c) flag = true;
				if (!flag) continue;


				strcpy(ans_id[num_of_ans], p.train_id);


				int p1 = -1, p2 = -1;
				for (auto j = 0; j < p.num_station; j++) {
					if (strcmp(p.station_name[j], it.getdata().first.loc1) == 0) p1 = j;
					if (strcmp(p.station_name[j], it.getdata().first.loc2) == 0) p2 = j;
				}

				strcat(ans[num_of_ans], it.getdata().first.train_id);
				strcat(ans[num_of_ans], " ");
				strcat(ans[num_of_ans], it.getdata().first.loc1);
				strcat(ans[num_of_ans], " ");
				char d[11] = "2018-06-";
				d[8] = int(date / 10) + '0';
				d[9] = int(date % 10) + '0';
				strcat(ans[num_of_ans], d);
				strcat(ans[num_of_ans], " ");
				strcat(ans[num_of_ans], p.start_time[p1]);
				strcat(ans[num_of_ans], " ");

				bool next_day = nextDay(p.start_time[p1], p.arrive_time[p2]);

				if (next_day) {
					if (d[9] - '9' == 0) { d[9] = '0', d[8]++; }
					else d[9]++;
				}
				strcat(ans[num_of_ans], it.getdata().first.loc2);
				strcat(ans[num_of_ans], " ");
				strcat(ans[num_of_ans], d);
				strcat(ans[num_of_ans], " ");
				strcat(ans[num_of_ans], p.arrive_time[p2]);
				float price;
				for (int j = 0; j < p.num_price; j++) {
					price = 0;
					for (int k = p1 + 1; k <= p2; k++) {
						price += p.price[k][j];
					}
					strcat(ans[num_of_ans], " ");
					strcat(ans[num_of_ans], p.ticket_kind[j]);
					strcat(ans[num_of_ans], " ");
					strcat(ans[num_of_ans], int_to_string(it.getdata().second.num[j]));
					strcat(ans[num_of_ans], " ");
					strcat(ans[num_of_ans], float_to_string(price));
				}
				num_of_ans++;
				if (it != buy_ticket_db.end()) break;
			}
		}
		return num_of_ans;
	}

	int refund_ticket(size_t user_id, int num, char *train_id, char *loc1,
		char *loc2, int date, char *ticket_kind) {
		auto t = buy_ticket_db.find(BuyTicketIndex(user_id, train_id, date, loc1, loc2));
		if (t == buy_ticket_db.end()) return 0;
		int ticket_kind_index;
		for (int i = 0; i < 5; i++) {
			if (strcmp(t.getdata().second.ticket_kind[i], ticket_kind) == 0) ticket_kind_index = i;
		}
		if (num > t.getdata().second.num[ticket_kind_index]) return 0;
		if (num == t.getdata().second.num[ticket_kind_index])
			buy_ticket_db.remove(t.getdata().first);
		else {
			auto tmp = t.getdata().second;
			tmp.num[ticket_kind_index] -= num;
			buy_ticket_db.update(t.getdata().first, tmp);
		}
		auto the_train = train_data_base.find(train_id).getdata().second;

		int p1 = -1, p2 = -1;
		for (int i = 0; i < the_train.num_station; i++) {
			if (strcmp(the_train.station_name[i], loc1) == 0) p1 = i;
			if (strcmp(the_train.station_name[i], loc2) == 0) p2 = i;
		}
		auto tmp =ticket_db.find(Ticket_i(date, the_train.train_id)).getdata().second;
		for (int i = p1; i < p2; i++) {
			// the_train.ticket[date][i][ticket_kind_index] += num;
			tmp.ticket[i][ticket_kind_index] += num;
		}
		if(p1 < p2) ticket_db.update(Ticket_i(date, the_train.train_id), tmp);
		return 1;
	}
};



#endif //UNTITLED_TRAIN_H
