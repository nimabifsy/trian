//
// Created by ziyu on 18-5-8.
//

#ifndef MYPROJECT_MYALGORITHM_H
#define MYPROJECT_MYALGORITHM_H
#include <fstream>
#include <iostream>
#include <cstring>

using namespace std;

struct String {
	char data[40];
	bool operator<(const String &b)const {
		return strcmp(this->data, b.data) > 0;
	}
	bool operator>(const String &o)const { return (o < *this); }
	bool operator>=(const String &o)const { return !(*this < o); }
	bool operator<= (const String &o)const { return !(*this > o); }
	bool operator!=(const String &o)const { return (*this < o) || (*this > o); }
	bool operator== (const String &o)const { return !(*this != o); }
	String(const char *d) {
		memset(data, 0, sizeof(data));
		strcpy(data, d);
	}
	String(const String &o) {
		memset(data, 0, sizeof(data));
		strcpy(data, o.data);
	}
	String operator=(const String &o) {
		memset(data, 0, sizeof(data));
		strcpy(data, o.data);
		return *this;
	}
	String() { memset(data, 0, 41); }
};

ostream &operator<<(ostream &os, String &a) {
	os << a.data;
	return os;
}

istream &operator>>(istream &is, String &b) {
	is >> b.data;
	return is;
}


#endif //MYPROJECT_MYALGORITHM_H
