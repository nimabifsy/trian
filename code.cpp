#include <iostream>
#include "DB.h"
#include "User.h"
#include "train.h"
#include <cstring>
using namespace std;

double string_to_double(char *p) {
	double ans1 = 0;
	bool flag = false;
	for (int i = 3; i < strlen(p); i++) {
		if (p[i] == '.') { flag = true; continue; }
		if (!flag) ans1 = ans1 * 10 + p[i] - '0';
		else {
			ans1 += (p[i] - '0') / 10.0;
			if (++i < strlen(p))
				ans1 += (p[i] - '0') / 100.0;
			break;
		}
	}
	return ans1;
}

char ch;
	char train_id[20], catalog[10], name[40];
	char ticket_kind[5][20];
	char station_name[50][40];
	char arrive_time[50][10];
	char start_time[50][10];
	char stop_over_time[50][10];
	float price[50][5] = { 0 };
	char non_price[50][5][10];
	int num_station, num_price;

TrainDB::Train readTrain() {
	
	memset(train_id, 0, 20);
	memset(catalog, 0, 10);
	memset(name, 0, 40);
	for (int i = 0; i < 5; i++)
		memset(ticket_kind[i], 0, 20);
	for (int i = 0; i < 50; i++) {
		memset(station_name[i], 0, 40);
		memset(arrive_time[i], 0, 10);
		memset(start_time[i], 0, 10);
		memset(stop_over_time[i], 0, 10);
	}
	cin >> train_id >> name >> catalog;
	cin >> num_station >> num_price;
	for (int i = 0; i < num_price; i++)
		cin >> ticket_kind[i];
	for (int i = 0; i < num_station; i++) {
		cin >> station_name[i] >> arrive_time[i] >> start_time[i] >> stop_over_time[i];
		for (int j = 0; j < num_price; j++) {	
			cin >> non_price[i][j];

			//cerr << non_price[i][j] << "\t";
			price[i][j] = string_to_double(non_price[i][j]);
			//cerr << price[i][j]<< endl;
		}
	}

	return TrainDB::Train(train_id, name, catalog, num_station, num_price, ticket_kind, station_name, arrive_time, start_time, stop_over_time, price);
}



int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	freopen("all.in", "r", stdin);
	// freopen("my_ans.out", "w", stdout);
	UserDB UserDatabase;
	TrainDB TrainDatabase;
	
	char cmd[20];
	// cerr<< sizeof(TrainDB::BuyTicketValue)<< endl;

	while (cin >> cmd) {
		if (strcmp(cmd, "clean") == 0) {
			cout << 1 << "\n";
		}
		else if (strcmp(cmd, "register") == 0) {
			char name[40];
			char password[21];
			char email[21];
			char phone[21];

			cin >> name >> password >> email >> phone;
			cout << UserDatabase.Register(name, password, email, phone) << "\n";
		}
		else if (strcmp(cmd, "login") == 0) {
			size_t id;
			char password[21];
			cin >> id >> password;
			cout << UserDatabase.Login(id, password) << "\n";
		}
		else if (strcmp(cmd, "query_profile") == 0) {
			size_t id;
			cin >> id;

			char ans[100];
			memset(ans, 0, sizeof(ans));
			if (UserDatabase.query_profile(id, ans) == 0) cout << 0 << "\n";
			else cout << ans << "\n";
		}
		else if (strcmp(cmd, "modify_profile") == 0) {
			size_t id;
			char name[40];
			char password[21];
			char email[21];
			char phone[21];
			cin >> id >> name >> password >> email >> phone;
			cout << UserDatabase.modify_profile(id, name, password, email, phone) << "\n";
		}
		else if (strcmp(cmd, "modify_privilege") == 0) {
			size_t id1, id2;
			int pri;
			cin >> id1 >> id2 >> pri;
			cout << UserDatabase.modify_privilege(id1, id2, (privilige)pri) << "\n";
		}
		else if (strcmp(cmd, "query_ticket") == 0) {
			char ans[200][300];
			char loc1[40], loc2[40], catalog[10], non_date[11];
			memset(loc1, 0, sizeof(loc1));
			memset(loc2, 0, sizeof(loc2));
			memset(catalog, 0, sizeof(catalog));
			memset(non_date, 0, sizeof(non_date));
			cin >> loc1 >> loc2 >> non_date >> catalog;
			int date = 10 * (non_date[8] - '0') + non_date[9] - '0';
			for (int i = 0; i < 100; i++)
				memset(ans[i], 0, sizeof(ans[i]));
			int n = TrainDatabase.query_ticket(loc1, loc2, date, catalog, ans);
			cout << n << "\n";
			for (int i = 0; i < n; i++) {
				cout << ans[i] << "\n";
			}
		}
		else if (strcmp(cmd, "query_transfer") == 0) {
			char ans[100][200];
			char loc1[40], loc2[40], catalog[10], non_date[11];
			memset(loc1, 0, sizeof(loc1));
			memset(loc2, 0, sizeof(loc2));
			memset(catalog, 0, sizeof(catalog));
			memset(non_date, 0, sizeof(non_date));
			cin >> loc1 >> loc2 >> non_date >> catalog;
			int date = 10 * (non_date[8] - '0') + non_date[9] - '0';

			if (TrainDatabase.query_transfer(loc1, loc2, date, catalog, ans) == -1) cout << "-1\n";
			else
				cout << ans[0] << "\n" << ans[1] << "\n";
		}
		else if (strcmp(cmd, "buy_ticket") == 0) {
			size_t id;
			int num;
			char train_id[20], loc1[40], loc2[40], catalog[10], non_date[11], ticket_kind[20];
			memset(train_id, 0, sizeof(train_id));
			memset(loc1, 0, 40);
			memset(loc2, 0, 40);
			memset(catalog, 0, 10);
			memset(non_date, 0, 11);
			memset(ticket_kind, 0, 20);
			cin >> id >> num >> train_id >> loc1 >> loc2 >> non_date >> ticket_kind;
			int date = 10 * (non_date[8] - '0') + non_date[9] - '0';
			cout << TrainDatabase.buy_ticket(id, num, train_id, loc1, loc2, date, ticket_kind) << "\n";
		}
		else if (strcmp(cmd, "query_order") == 0) {
			char ans[100][200];
			size_t id;
			char non_date[11], catalog[10];
			memset(non_date, 0, 11);
			memset(catalog, 0, 10);
			cin >> id >> non_date >> catalog;

			int date = 10 * (non_date[8] - '0') + non_date[9] - '0';

			for (int i = 0; i < 100; i++)
				memset(ans[i], 0, 200);
			int n = TrainDatabase.query_order(id, date, catalog, ans);
			cout << n << "\n";
			for (int i = 0; i < n; i++) {
				cout << ans[i] << "\n";
			}
		}
		else if (strcmp(cmd, "refund_ticket") == 0) {
			size_t id;
			int num;
			char train_id[20], loc1[40], loc2[40], catalog[10], non_date[11], ticket_kind[20];
			memset(train_id, 0, 20);
			memset(loc1, 0, 40);
			memset(loc2, 0, 40);
			memset(catalog, 0, 10);
			memset(non_date, 0, 11);
			memset(ticket_kind, 0, 20);
			cin >> id >> num >> train_id >> loc1 >> loc2 >> non_date >> ticket_kind;
			int date = 10 * (non_date[8] - '0') + non_date[9] - '0';
			cout << TrainDatabase.refund_ticket(id, num, train_id, loc1, loc2, date, ticket_kind) << "\n";
		}
		else if (strcmp(cmd, "add_train") == 0) {
			cout << TrainDatabase.add_train(readTrain()) << "\n";
		}
		else if (strcmp(cmd, "sale_train") == 0) {
			char train_id[20];
			memset(train_id, 0, 20);
			cin >> train_id;
			cout << TrainDatabase.sale_train(train_id) << "\n";
		}
		else if (strcmp(cmd, "query_train") == 0) {
			char train_id[20];
			memset(train_id, 0, 20);
			cin >> train_id;
			TrainDatabase.query_train(train_id);
		}
		else if (strcmp(cmd, "delete_train") == 0) {
			char train_id[20];
			memset(train_id, 0, 20);
			cin >> train_id;
			cout << TrainDatabase.delete_train(train_id) << "\n";
		}
		else if (strcmp(cmd, "modify_train") == 0) {
			cout << TrainDatabase.modify_train(readTrain()) << "\n";
		}
		// else if(cmd == "clean") cout << clean()<< "\n";
		else if (strcmp(cmd, "exit") == 0) { cout << "BYE" << "\n"; }
		else cerr << "fuck you" << "\n";
	}
	return 0;
}
